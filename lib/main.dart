import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';


void main() {
  runApp(MaterialApp(
    home: MyApp(),
    debugShowCheckedModeBanner: false,
  ));
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 3,
      initialIndex: 0,
      child: Scaffold(
        appBar: AppBar(
          title: Image.asset(
            'assets/pic1.png',
            height: 30.0,
          ),
          centerTitle: true,
          backgroundColor: Colors.blueGrey[100],
          elevation: 0.0,
        ),
        body: Container(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: const EdgeInsets.all(20.0),
                child: Container(
                  height: 45.0,
                  width: MediaQuery.of(context).size.width,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(40),
                    color: Colors.grey[200],
                    // border: Border.all(width: 2.5, color: Colors. lightBlue),
                  ),
                  child: Padding(
                    padding: const EdgeInsets.fromLTRB(25.0, 0.0, 0, 0),
                    child: Row(
                      children: [
                        Image.asset('assets/Search icon.png'),
                        SizedBox(
                          width: 20.0,
                        ),
                        Text(
                          'Adele',
                          style: TextStyle(
                            color: Colors.grey,
                            fontSize: 20.0,
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
              TabBar(
                  indicatorSize: TabBarIndicatorSize.label,
                  unselectedLabelColor: Colors.black,
                  labelColor: Colors.lightBlue,
                  tabs: [
                    Tab(
                      child: Text(
                        'Posts',
                        style: TextStyle(
                          fontSize: 18.0,
                        ),
                      ),
                    ),
                    Tab(
                      child: Text(
                        'Accounts',
                        style: TextStyle(
                          fontSize: 18.0,
                        ),
                      ),
                    ),
                    Tab(
                      child: Text(
                        'Places',
                        style: TextStyle(
                          fontSize: 18.0,
                        ),
                      ),
                    ),
                  ]),
              Padding(
                padding: const EdgeInsets.only(left: 48.0, right: 48.0, top: 0),
                child: Divider(color: Colors.grey,
                  thickness: 2,),
              ),
              Expanded(child: TabBarView(
                children: [
                  Padding(
                    padding: const EdgeInsets.all(23.0),
                    child: SingleChildScrollView(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          // ignore: missing_return
                          ListTile(
                            leading: CircleAvatar(
                              backgroundImage: AssetImage('assets/images/Ellipse 28.png'),
                            ),
                            title: Text('Adele Abe', style: TextStyle(
                              fontSize: 17,
                            ),),
                          ),
                          SizedBox(height: 15.0,),
                          ListTile(
                            leading: CircleAvatar(
                              backgroundImage: AssetImage('assets/images/Ellipse 28 (1).png'),
                            ),
                            title: Text('Adele Azouli', style: TextStyle(
                              fontSize: 17,
                            ),),
                          ),
                          SizedBox(height: 15.0,),
                          ListTile(
                            leading: CircleAvatar(
                              backgroundImage: AssetImage('assets/images/Ellipse 29.png'),
                            ),
                            title: Text('Adele Berdon', style: TextStyle(
                              fontSize: 17,
                            ),),
                          ),
                          SizedBox(height: 15.0,),
                          ListTile(
                            leading: CircleAvatar(
                              backgroundImage: AssetImage('assets/images/Ellipse 30.png'),
                            ),
                            title: Text('Adele Bertrand', style: TextStyle(
                              fontSize: 17,
                            ),),
                          ),
                          SizedBox(height: 15.0,),
                          ListTile(
                            leading: CircleAvatar(
                              backgroundImage: AssetImage('assets/images/Ellipse 31.png'),
                            ),
                            title: Text('Adele Boulin', style: TextStyle(
                              fontSize: 17,
                            ),),
                          ),
                          SizedBox(height: 15.0,),
                          ListTile(
                            leading: CircleAvatar(
                              backgroundImage: AssetImage('assets/images/Ellipse 32.png'),
                            ),
                            title: Text('Adele Colin', style: TextStyle(
                              fontSize: 17,
                            ),),
                          ),
                          SizedBox(height: 15.0,),
                          ListTile(
                            leading: CircleAvatar(
                              backgroundImage: AssetImage('assets/images/Ellipse 33.png'),
                            ),
                            title: Text('Adele Cloth', style: TextStyle(
                              fontSize: 17,
                            ),),
                          ),
                          SizedBox(height: 15.0,),
                          ListTile(
                            leading: CircleAvatar(
                              backgroundImage: AssetImage('assets/images/Ellipse 34.png'),
                            ),
                            title: Text('Adele Daniel', style: TextStyle(
                              fontSize: 17,
                            ),),
                          ),
                          SizedBox(height: 15.0,), ListTile(
                            leading: CircleAvatar(
                              backgroundImage: AssetImage('assets/images/Ellipse 35.png'),
                            ),
                            title: Text('Adele Dilan', style: TextStyle(
                              fontSize: 17,
                            ),),
                          ),
                          SizedBox(height: 15.0,),
                        ],
                      ),
                    ),
                  ),Column(
                    children: [
                      Icon(Icons.add),
                    ],
                  ),Column(
                    children: [],
                  ),
                ],
              ))
            ],
          ),
        ),
        bottomNavigationBar: Container(
          height: 70.0,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.only(
                topRight: Radius.circular(15), topLeft: Radius.circular(15.0)),
          ),
          child: ClipRRect(
            borderRadius: BorderRadius.only(
                topRight: Radius.circular(15), topLeft: Radius.circular(15.0)),
            child: BottomNavigationBar(
              items: [
                BottomNavigationBarItem(
                    icon: Image.asset('assets/House icon.png'),
                    title: Text(''),
                    backgroundColor: Color(0xff01024E)),
                BottomNavigationBarItem(
                    icon: Image.asset('assets/Pet icon.png'),
                    title: Text(''),
                    backgroundColor: Color(0xff01024E)),
                BottomNavigationBarItem(
                    icon: Image.asset('assets/World icon.png'),
                    title: Text(''),
                    backgroundColor: Color(0xff01024E)),
                BottomNavigationBarItem(
                    icon: Image.asset('assets/Account icon.png'),
                    title: Text(''),
                    backgroundColor: Color(0xff01024E)),
              ],
            ),
          ),
        ),
      ),
    );
  }
}