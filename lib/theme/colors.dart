
import 'dart:core';
import 'dart:ui';

import 'package:flutter/material.dart';

const Color primaryColor = Color.fromARGB(1,1,2,78);
const Color textColor = Color(0xffE5E6E8);
const Color dotColor = Color(0xff6477DC);
const Color backGround = Color(0xffF8F8FC);